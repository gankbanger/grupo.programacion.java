package edu.java.programacion;

public class App {

	public static void main(String[] args) {
		ValidadorISBN validador = new ValidadorISBN();
		for (String isbn : args) {
			System.out.println(isbn + (validador.esValido(isbn)? " es válido" : " no es válido"));
		}
	}

}
