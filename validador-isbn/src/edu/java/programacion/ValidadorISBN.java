package edu.java.programacion;

/**
 * Validador de códigos ISBN de diez dígitos
 * @author gank
 *
 */
public class ValidadorISBN {

	/**
	 * Retorna verdadero si el argumento isbn es válido. Es decir, la suma de sus dígitos no
	 * es un múltiplo de 11.
	 * @param isbn Un isbn de diez dígitos
	 * @return verdadero si el isbn es válido
	 */
	public boolean esValido(final String isbnOriginal) throws IllegalArgumentException {
    	String isbn = limpiar(isbnOriginal);		
		if (!verificarFormato(isbn)) {
		    return false;
		}

		int suma = 0;
		for (int i = 0; i < 10; i++){
			suma += (10 -i) * valorDe(isbn.charAt(i)); 
		}
		return (suma % 11 == 0); // ¿Es múltiplo de 11?
	}
	
	private int valorDe(char digito) {
	    return (digito != 'X') ? Character.getNumericValue(digito) : 10; // 'X' vale 10
	}

	private boolean verificarFormato(final String isbn) {
		return (isbn.matches("([0-9]{10})|([0-9]{9}X)"));
	}

	private String limpiar(final String isbn) {
		return isbn.replaceAll(" ", "").replaceAll("-", "").toUpperCase();
	}
}
