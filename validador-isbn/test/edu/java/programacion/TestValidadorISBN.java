package edu.java.programacion;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestValidadorISBN {

	private ValidadorISBN validador = new ValidadorISBN();
	@Test
	public void testISBNValido() {
		assertTrue(validador.esValido("0-7475-3269-9"));
	}
	
	@Test
	public void testISBNValidoConX() {
		assertTrue(validador.esValido("3-04-013311-X"));
	}
	
	@Test
	public void testISBNInvalido() {
		assertFalse(validador.esValido("0-7475-3269-8"));
	}
	
	@Test
	public void testISBNInvalidoConX() {
		assertFalse(validador.esValido("3-04-013341-X"));
	}
	
	@Test
	public void testISBNMalFormateado() {
    	assertFalse(validador.esValido("4-X4-013341-X"));
	}

}
