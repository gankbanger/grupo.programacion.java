import java.util.*;
import java.lang.*;

public class Romanos {
    public static void main(String[] args) {
        for (String arg : args) {
            try {
                System.out.println(arg + " = " + Romano.aRomano(Integer.valueOf(arg)));
            } catch (NumberFormatException e) {
                System.out.println(arg + " = " + Romano.aDecimal(arg));
            }
        }        
    }
    
    public enum Romano {
        I(1, "I"), 
        IV(4, "IV"), 
        V(5, "V"), 
        IX(9, "IX"), 
        X(10, "X"), 
        XL(40, "XL"), 
        L(50, "L"), 
        XC(90, "XC"), 
        C(100, "C"), 
        CD(400, "CD"), 
        D(500, "D"), 
        CM(900, "CM"), 
        M(1000, "M");
        
        private static List<Romano> romanosValorAscendente = new ArrayList<Romano>(Arrays.asList(Romano.M, 
                                                                                                 Romano.CM, 
                                                                                                 Romano.D, 
                                                                                                 Romano.CD, 
                                                                                                 Romano.C, 
                                                                                                 Romano.XC, 
                                                                                                 Romano.L, 
                                                                                                 Romano.XL, 
                                                                                                 Romano.X, 
                                                                                                 Romano.IX, 
                                                                                                 Romano.V, 
                                                                                                 Romano.IV, 
                                                                                                 Romano.I));
        
        private static List<Romano> romanosTextoAscendente = new ArrayList<Romano>(Arrays.asList(Romano.IV, 
                                                                                                 Romano.IX, 
                                                                                                 Romano.XL, 
                                                                                                 Romano.XC, 
                                                                                                 Romano.CD, 
                                                                                                 Romano.CM, 
                                                                                                 Romano.I, 
                                                                                                 Romano.V, 
                                                                                                 Romano.X, 
                                                                                                 Romano.L, 
                                                                                                 Romano.C, 
                                                                                                 Romano.D, 
                                                                                                 Romano.M));        
        
        private int valor;
        private String texto;
        
        private Romano(final int valorDecimal, final String textoRomano) {
            this.valor = valorDecimal;
            this.texto = textoRomano;
        }
        
        public int obtenerValor() {
            return this.valor;
        }
        
        public String aTexto() {
            return this.texto;
        }
        
        public static int aDecimal(final String romano) {
            int decimal = 0;
            int posicion = 0;
            while (posicion < romano.length()){
                for (Romano digitoRomano : romanosTextoAscendente) {
                    if (romano.startsWith(digitoRomano.aTexto(), posicion)) {
                        posicion += digitoRomano.aTexto().length();
                        decimal += digitoRomano.obtenerValor();
                    }
                }
            }
            return decimal;
        }
        
        public static String aRomano(int decimal) {
            StringBuilder romanos = new StringBuilder();
            while (decimal > 0) {
                for (Romano romano : romanosValorAscendente) {
                    if (romano.obtenerValor() <= decimal) {
                        romanos.append(romano.aTexto());
                        decimal -= romano.obtenerValor();
                        break;
                    }
                }
            }
            return romanos.toString();
        }
    }
}
